This is a simple game about dividing numbers.

Install with "pip install divisor".
Run with "divisor".
Then, try to come up with numbers that evenly divide the number given.  1 and the number itself are illegal.
