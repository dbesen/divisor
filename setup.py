from distutils.core import setup
setup(
    name = 'divisor',
    scripts = ['divisor'],
    version = '0.14',
    description = 'A simple game about dividing numbers.',
    author = 'David Besen',
    author_email = 'dbesen@gmail.com',
    url = 'https://bitbucket.org/dbesen/divisor',
    keywords = ['game', 'cli', 'math'],
    classifiers = [],
)
